Scavenger Mobile
====

## Getting Started
To start with install the ionic CLI (Command line interface):

    npm install -g ionic
    npm insatll -g bower

    ionic start scavenger

Enter "Y" to create a new ionic account and create the account on the web site. Create a new application named scavenger in your account.

Note the commands that ionic provides in the initial output after installation. These will be helpful later:

 * cd into your project: $ cd scavenger
 * Setup this project to use Sass: ionic setup sass
 * Develop in the browser with live reload: ionic serve
 * Add a platform (ios or Android): ionic platform add ios [android]
   Note: iOS development requires OS X currently
   See the Android Platform Guide for full Android installation instructions:
   https://cordova.apache.org/docs/en/edge/guide_platforms_android_index.md.html
 * Build your app: ionic build <PLATFORM>
 * Simulate your app: ionic emulate <PLATFORM>
 * Run your app on a device: ionic run <PLATFORM>
 * Package an app using Ionic package service: ionic package <MODE> <PLATFORM>

Add this to index.html
====
    <script src="https://cdn.firebase.com/js/client/2.2.4/firebase.js"></script>
    <script src="https://cdn.firebase.com/libs/angularfire/1.2.0/angularfire.min.js"></script>

Create Database Connection
====
    var db = new Firebase("https://fusescavenger.firebaseio.com/");
    db.authWithCustomToken("awflHFrN6gU5KYtBrmRLqygjMZbTp9w9H2IzfczI", function (){ });

Read From Database
====
    db.on("value", function(allDbHunts) {
      var allHunts = allDbHunts.val();
      for (var dbHunt in allHunts) {
        if (allHunts.hasOwnProperty(dbHunt)) {
          $scope.hunts.push(allHunts[dbHunt]);
        }
      }

      $scope.$apply();
    });

Write to the database
====
    function authenticateFirebase(db) {
      // TODO: Move this somewhere else
      var AUTH_TOKEN = "awflHFrN6gU5KYtBrmRLqygjMZbTp9w9H2IzfczI";
      db.authWithCustomToken(AUTH_TOKEN, function(error, authData) {
        if (error) console.log("Login Failed!", error);
        else console.log("Login Succeeded!", authData);
      });
    }

    var db = new Firebase("https://fusescavenger.firebaseio.com/");
    authenticateFirebase(db);

    var dbHunt = db.child($scope.hunt.huntName);
    dbHunt.set(‘<objectToSaveGoesHere>’, resetTheScreen);


