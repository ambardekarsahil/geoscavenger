var services = angular.module('starter.services', []);

services.factory('Chats', function() {
  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});

services.factory('Hunts', function() {
    return {

        currentHunt: {},

        getCurrentHunt: function() {

            return currentHunt;
        },

        setCurrentHunt: function(hunt) {

            currentHunt = hunt;
        },

        get: function(resolve) {
        
        
            var db = new Firebase("https://fusescavenger.firebaseio.com/hunts");
            db.authWithCustomToken("awflHFrN6gU5KYtBrmRLqygjMZbTp9w9H2IzfczI", function (){ });     
                    
            db.on("value", function(allDbHunts) {
                var allHunts = allDbHunts.val();
                resolve(allHunts);
            });
        
                  
        }
    }
});
