Scavenge
====

**Core Objective**

Build a scavenger hunt game for mobile devices in [ionic framework](http://ionicframework.com/).


**The Details**

The end goal of this project is to create a usable scavenger hunt game using ionic. We have a sample, but you come up with the requirements for how the game looks, feels and works! Given that ionic framework is based on angular this should allow learning:

* Ionic
* Javascript
* Angular
* Mobile application development
* Github
* HTML5 and custom HTML tags
* Firebase

By the end of the day, even if you don't have a full knowledge of all these technologies you should have the background to navigate and build your own apps!

Additionally we will try to work through building this software the way real software is built in the wild. This will therefore be an excellent primer for developing software in a shared environment from requirements to actual production code. If you do already know angular or Javascript this will be of great help. Also having an android or iOS device should also be helpful.

**Coaches**

Andrew Waldron and Jacob Hanning
